.\" Copyright (C) 2002  Ben Kibbey <bjk@arbornet.org>
.\" 
.\" This program is free software; you can redistribute it and/or modify
.\" it under the terms of the GNU General Public License as published by
.\" the Free Software Foundation; either version 2 of the License, or
.\" (at your option) any later version.
.\" 
.\" This program is distributed in the hope that it will be useful,
.\" but WITHOUT ANY WARRANTY; without even the implied warranty of
.\" MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
.\" GNU General Public License for more details.
.\" 
.\" You should have received a copy of the GNU General Public License
.\" along with this program; if not, write to the Free Software
.\" Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
.
.Dd October 08, 2002
.Os
.Dt BUBBLEGUM 8
.
.Sh NAME
.
.Nm bubblegum
.Nd watch files for changes
.
.Sh SYNOPSIS
.
.Nm bubblegum
.Op Fl hvksq
.Fl MAamcE
.Op Fl l Ar logfile
.Op Fl Xo 0|1|2 Xc
.Oo
.Op Fl e Ar command
.Op Fl r Ar count | Fl t Ar count
.Oc
.Op Fl n Ar niceness
.Op Fl i Ar seconds
.Op Fl p Ar pidfile
.Ar
|
.Fl f Ar filelist
.
.Sh DESCRIPTION
.
.Nm bubblegum
is a daemon which watches a files access, modification and inode change times
via
.Fn stat
and can also check MD5 checksums if
.Em libmd
is available at compile-time. Filenames obtained from a file list via the 
.Fl f
option or from the command line are relative to the current directory unless
they begin with a 
.Sq \&/ .
.Pp
If a file specified on the command line or in a file list is a directory and
ends with a
.Sq \&/ ,
then the directory will be recursed having all files and subdirectories added
to the list of files to watch. Note that this will modify the access time of
the directories when loading or reloading a file list or on startup.
.
.Pp
The following command line options are available:
.
.Bl -tag -width
.It Fl f Ar filename
Obtain a list of files to watch, one per line, from
.Ar filename .
.It Fl n Ar level
The priority or niceness level,
.Ar \&-20
through
.Ar 20 ,
of the daemon process and executed commands. Only the superuser can set a
priority lower than the parent process.
.It Fl i Ar seconds
Interval in seconds to wait between file checks. Must be greater than zero.
The default is
.Ar 1 .
.It Fl a
Check the acces time of the file.
.It Fl m
Check the modification time of the file.
.It Fl c
Check the inode change time of the file.
.It Fl M
Check the MD5 checksum of the file. The files checksum is updated after
detecting a change. The files modification time is used as the time
difference. Note that both MD5 and access time checking
.Fl a
cannot be specified because MD5 checking modifies the access time. Also note
that this option increases CPU overhead significantly, escpecially with a
short time interval and lots of files. This option is only available if
compiled with 
.Em libmd
support.
.It Fl E
Check for errors when accessing the file via
.Fn stat
or
.Fn MD5File .
.It Fl A
Check everything except MD5 checksums. This option is a shortcut for
specifying
.Fl amcE .
.It Fl e Ar command
Execute a command via 
.Fn execvp
after detecting a file change. Any arguments to 
.Ar command
which contain spaces must be enclosed in double quotes
.Sq \&"
to keep the
argument list sane. To include a double quote or backslash character in an
argument itself, precede it with a backslash
.Sq \e
character. See
.Sx EXPANSIONS 
below for special character expansions.
.It Fl r Ar count
Maximum number of times to run
.Ar command 
for each file. A setting of 
.Ar 0 
will run
.Ar command
an infinite number of times. The default is
.Ar 1 .
.It Fl t Ar count
Total number of times to run 
.Ar command
for all files.
This option overrides
.Fl r .
.It Fl l Ar logfile
Log changes to the specified 
.Ar logfile . 
The default is
.Pa ~/bubblegum.log .
See
.Sx ENVIRONMENT
and
.Sx FILES ,
below.
.It Fl Xo 0|1|2 Xc
Logging level.
.Fl Xo 0 Xc
will disable file logging.
.Fl Xo 1 Xc 
will log only the first change of each file.
.Fl Xo 2 Xc
will log all changes of all files up to an error if error checking is enabled.
If the error changes, then the error is logged. If the error disappears, then
logging for that file continues as normal. The default is
.Fl Xo 2 Xc .
.It Fl s
Enable logging to
.Xr syslogd 8
and disable file logging. Note that the logging level still applies.
.It Fl k
If a file is a symbolic link, check the link itself, not the file the link
follows.
.It Fl p Ar pidfile
An alternate process id file to use. The default is
.Pa ~/bubblegum.pid .
See
.Sx ENVIRONMENT
and
.Sx FILES ,
below.
.It Fl q
Terminate an already running daemon.
.It Fl h
Program help text.
.It Fl v
Version and copyright information.
.El
.
.Sh "EXPANSIONS"
When specifying a command with the
.Fl e
command line option, special character expansions are available to show
information about a files change. The following expansions are available and
each should be enclosed in double quotes
.Sq \&"
to keep the argument list sane
(see
.Sx EXAMPLES ,
below):
.Pp
.Bl -tag -compact -width \&%\&%
.It Cm \&%f
Full path of the filename.
.It Cm \&%F
The type of file:
.Em DIRECTORY ,
.Em FIFO ,
.Em LINK ,
.Em CHARACTER ,
.Em REGULAR ,
.Em BLOCK
or
.Em SOCKET .
.It Cm \&%m
File permission mode in octal.
.It Cm \&%t
Time stamp of the latest change.
.It Cm \&%b
Time of the last change.
.It Cm \&%d
Time stamp of when the change was detected.
.It Cm \&%i
Time difference of the latest and last change in
.Em type:days:hours:minutes:seconds 
format where
.Em type
is one of
.Em A ,
.Em M
or
.Em C 
for access, modification or inode change time respectively.
.It Cm \&%s
Daemon status.
.It Cm \&%p
Daemon process id.
.It Cm \&%c
A comma separated list of changes:
.Em ATIME ,
.Em MTIME , 
.Em CTIME ,
.Em MD5
or
.Em ERROR .
.It Cm \&%y
A comma separated list change types, if available. The recognized
change types are: 
.Em IDEV ,
.Em INODE ,
.Em HLINK ,
.Em USER ,
.Em GROUP ,
.Em PERM 
and
.Em SIZE .
If no change type is available then a single
.Sq \&-
is used. See 
.Xr stat 2
for definitions of the change types.
.It Cm \&%e
If an error occurs while accessing the file with
.Fn stat
or
.Fn MD5File
and error checking has been enabled with the
.Fl E
command line option, then this is expanded to the error string via
.Fn strerror .
Otherwise it holds a single
.Sq \&-
character.
.It Cm \&%r
The number of times
.Ar command 
has been run for the current file and the total number of times
allowed specified with the
.Fl r
or
.Fl t
command line options.
.It Cm \&%\&%
A regular
.Sq \&%
character.
.El
.
.Sh "SIGNALS"
.Bl -tag -compact -width SIGTERM
.It SIGALRM
Force the current timer to expire causing the next file check.
.It SIGHUP
Reload the file list specified with the 
.Fl f
command line option keeping the original changed bits and command runs for
existing files.
.It SIGUSR1
Reset only the changed bits for all files.
.It SIGUSR2
Reset changed bits and command runs for all files.
.It SIGTERM
Terminate the daemon.
.El
.
.Sh ENVIRONMENT
.Bl -tag -width BUBBLEGUM_LOG -compact
.It Ev BUBBLEGUM_LOG
Logging file. Command line option
.Fl l
will override this variable.
.It Ev BUBBLEGUM_PID
Process id file. Command line option
.Fl p
will override this variable.
.El
.
.Sh FILES
.Bl -tag -compact -width ~/bubblegum.log
.It Pa ~/bubblegum.log
Default logging file.
.It Pa ~/bubblegum.pid
Default process id file.
.El
.
.Sh EXAMPLES
.Dl \&% bubblegum \&-e \&'script.sh \&"\&%f\&" \&"\&%c\%" \&"\&%y\&"\&' \&-As \&-f filelist
.
.Sh SEE ALSO
.Xr stat 2 ,
.Xr MD5File 3 ,
.Xr setpriority 2 ,
.Xr fork 2 ,
.Xr select 2 ,
.Xr syslogd 8 ,
.Xr execvp 3
.
.Sh AUTHORS
.
.An "Ben Kibbey" Aq bjk@arbornet.org
