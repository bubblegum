/*
    Copyright (C) 2002  Ben Kibbey <bjk@arbornet.org>

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/stat.h>

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#ifdef HAVE_STRINGS_H
#include <strings.h>
#endif

#ifdef HAVE_ERR_H
#include <err.h>
#endif

#include "common.h"

#ifdef WANT_DMALLOC
#include <dmalloc.h>
#endif

FILES *filecmp(const char *filename, FILES * list)
{
    FILES *cur, *next;

    for (cur = list; cur; cur = next) {
	next = cur->next;

	if (!cur->filename)
	    break;

	if (strcmp(filename, cur->filename) == 0)
	    return cur;
    }

    return NULL;
}

int duplicate(const char *filename, FILES * list)
{
    FILES *cur, *next;

    for (cur = list; cur; cur = next) {
	next = cur->next;

	if (!cur->filename)
	    break;

	if (strcmp(filename, cur->filename) == 0)
	    return 1;
    }

    return 0;
}

FILES *loadfile(const char *filename, FILES * oldlist)
{
    FILE *fp;
    char *cp, buf[LINE_MAX];
    int err, oldtotal = total;
    FILES *fptr, *fhead;

    total = 0;

    if (oldlist && (err = is_file(filenamelist)) != 0) {
	if (err < 0)
	    logit("%s: %s", filenamelist, strerror(errno));
	else
	    logit("%s: Not a regular file", filenamelist);

	return NULL;
    }

    if (!(fp = fopen(filename, "r"))) {
	if (oldlist)
	    logit("%s: %s", filenamelist, strerror(errno));
	else
	    warn("%s", filenamelist);

	return NULL;
    }

    fhead = Calloc(1, sizeof(FILES));
    fptr = fhead;

    while ((cp = fgets(buf, sizeof(buf), fp)) != NULL) {
	struct stat st;
	FILES *cur;
	char tmp[FILENAME_MAX];

	cp[strlen(cp) - 1] = 0;
	cp = fullpath(wd, cp, tmp, sizeof(tmp));

/*        make sure there are no duplicate files in the list*/
	if (duplicate(cp, fhead)) {
	    fptr->next = NULL;

	    if (oldlist)
		logit("%s: Duplicate file", cp);
	    else
		warnx("%s: Duplicate file", cp);

	    continue;
	}

/*        keep old stat() and bit data if filenames are the same*/
	if (oldlist) {
	    if ((cur = filecmp(cp, oldlist)) != NULL) {
		bcopy(cur, fptr, sizeof(FILES));
		fptr->filename = strdup(cur->filename);
		fptr->next = Calloc(1, sizeof(FILES));
		fptr = fptr->next;

		total++;
		continue;
	    }
	}

	if (STAT(cp, &st) == -1) {
	    if (oldlist)
		logit("%s: %s", cp, strerror(errno));
	    else
		warn("%s", cp);

	    continue;
	}

	if (S_ISDIR(st.st_mode) && cp[strlen(cp) - 1] == '/') {
	    fptr = recurse_directory(fptr, cp, (oldlist) ? 1 : 0);
	    stat(cp, &st);
	}

	fptr = add_file(fptr, cp, st, (oldlist) ? 1 : 0);
	fptr = fptr->next;
    }

    fptr->next = NULL;
    fclose(fp);

    if (!total && oldlist) {
	total = oldtotal;
	logit("%s: No files", filenamelist);
	free(fhead);
	return NULL;
    }

    return fhead;
}
