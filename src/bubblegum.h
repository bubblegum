/*
    Copyright (C) 2002  Ben Kibbey <bjk@arbornet.org>

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/
#ifdef HAVE_DIRENT_H
#include <dirent.h>
#else
#define dirent direct
#ifdef HAVE_SYS_NDIR_H
#include <sys/ndir.h>
#endif
#ifdef HAVE_SYS_DIR_H
#include <sys/dir.h>
#endif
#ifdef HAVE_NDIR_H
#include <ndir.h>
#endif
#endif

#ifndef ALLPERMS
#define ALLPERMS        (S_ISUID|S_ISGID|S_IRWXU|S_IRWXG|S_IRWXO)
#endif

#define COPYRIGHT 	"Copyright (c) 2002 Ben Kibbey <bjk@arbornet.org>"
#define MAX_TIME_LEN 	64
#define NONE_STR	"-"
#define MD5_STR		"MD5"
#define ATIME_STR	"ATIME"
#define MTIME_STR	"MTIME"
#define CTIME_STR	"CTIME"
#define ERROR_STR	"ERROR"
#define IDEV_STR	"IDEV"
#define INODE_STR	"INODE"
#define SIZE_STR	"SIZE"
#define UID_STR		"UID"
#define GID_STR		"GID"
#define HLINKS_STR	"HLINK"
#define PERMS_STR	"PERM"
#define DIR_STR		"DIRECTORY"
#define CHR_STR		"CHARACTER"
#define BLK_STR		"BLOCK"
#define REG_STR		"REGULAR"
#define FIFO_STR	"FIFO"
#define LNK_STR		"LINK"
#define SOCK_STR	"SOCKET"

#ifdef HAVE_LIBMD
const char *optstr = "MqAn:t:p:skhvr:aEe:i:mf:l:c012";
#else
const char *optstr = "qAn:t:p:skhvr:aEe:i:mf:l:c012";
#endif
unsigned usesyslog, runlevel;
int niceness, loglevel;
long interval;
char *logfile, *pidfile, diffwhich;
char command[FILENAME_MAX];
pid_t pid;

FILES *loadfile(const char *filename, FILES * oldlist);
int pathlength(const char *wd, const char *filename);
