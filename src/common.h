/*
    Copyright (C) 2002  Ben Kibbey <bjk@arbornet.org>

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/
#ifndef LINE_MAX
#ifdef _POSIX2_LINE_MAX
#define LINE_MAX	_POSIX2_LINE_MAX
#elif defined(_POSIX_LINE_MAX)
#define LINE_MAX	_POSIX_LINE_MAX
#else
#define LINE_MAX	2048
#endif
#endif

#define SET_BIT(var, bit)	(var |= (1 << bit))
#define RESET_BIT(var, bit)	(var &= ~(1 << bit))
#define TEST_BIT(var, bit)	(var & (1 << bit))
#define TOGGLE_BIT(var, bit)	(var ^= (1 << bit))
#define STAT(file, sstat) ((symlinks) ? lstat(file, sstat) : stat(file, sstat))

#ifndef HAVE_ERR_H
#define err(eval, fmt, args...)		dumperr(eval, 1, fmt, ## args)
#define errx(eval, fmt, args...)	dumperr(eval, 0, fmt, ## args)
#define warn(fmt, args...)		dumperr(-1, 1, fmt, ## args)
#define warnx(fmt, args...)		dumperr(-1, 0, fmt, ## args)
#endif

enum { ACCESS, MODIFY, CHANGE, ERROR, MD5, MAXCHGBITS };
enum { IDEV, INODE, SIZE, UID, GID, HLINKS, PERMS, MAXTYPEBITS };
enum { NORMAL, RESET, RESETALL, RELOAD, QUIT };

typedef struct filelist {
    char *filename;
#ifdef HAVE_LIBMD
    char *md5sum;
    unsigned md5errno;
#endif
    struct stat laststat;
    unsigned staterrno;
    unsigned changed;
    unsigned chgtype;
    unsigned cmdruns;
    struct filelist *next;
} FILES;

char *filenamelist, *wd, *pn;
unsigned symlinks, check, total;

int is_file(const char *filename);
void logit(const char *format, ...);
void *Calloc(size_t number, size_t size);
void *Malloc(size_t size);
char *fullpath(const char *wd, char *filename, char dest[], size_t size);
FILES *recurse_directory(FILES *list, char *filename, int old);
FILES *add_file(FILES *list, char *filename, struct stat st, int old);
#ifndef HAVE_ERR_H
void dumperr(int eval, int verb, const char *fmt, ...);
#endif
