#!/bin/sh -
#
# Mail results of a file change from bubblegum. Put this script somewhere in
# your PATH and run with -h for more info.
#
# Ben Kibbey <bjk@arbornet.org>
##############################################################################
PN="`basename $0`";

usage () {
    cat <<EOF 1>&2
$PN - Mail the results of a file change from bubblegum.
Usage: $PN [-h] [-x <command>] [address] | <expansions>
    -x  Append the output of the specified command to the message body.
    -h  This help text.

If only one argument is specified then this argument is used as the address to
mail the results to. If no arguments are specified, the environment variable
LOGNAME@localhost is used as the address. Otherwise, arguments are assumed to
be the output of this script used as a bubblegum command.

Example: bubblegum -e "\`$PN -x 'users' user@some.host\`" ...
EOF

    exit 1;
}

while getopts "hx:" opt; do
    case "$opt" in
	x) CMD=$OPTARG;;
	h) usage;;
	\?) usage;;
	--) break;;
    esac;
done;

shift $(($OPTIND - 1));

FILE="$1";
TIME="$2";
BTIME="$3";
DTIME="$4";
DIFF="$5";
STATUS="$6";
RUNS="$7";
PID="$8";
CHANGE="$9";
TYPE="${10}";
ERROR="${11}";
FTYPE="${12}";
MODE="${13}";

if [ $# -le 1 ]; then
    echo -n "$0 ";

    if [ "$CMD" ]; then
        echo -n "-x \"$CMD\" ";
    fi;

    echo -n '"%f" "%t" "%b" "%d" "%i" "%s" "%r" "%p" "%c" "%y" "%e" ';
    echo -n '"%F" "%m" ';
    
    if [ ! "$1" ]; then
        echo "$LOGNAME@localhost";
    else
        echo "$1";
    fi;

    exit 0;
fi;


shift 13;

mail -s "bubblegum: $FILE" $* <<EOF
Daemon Status.........: $STATUS
Daemon PID............: $PID
Run Number/Total......: $RUNS
Filename..............: $FILE
File Type (Mode)......: $FTYPE ($MODE)
Time of Detection.....: $DTIME
Time of Last Change...: $BTIME
Time of Change........: $TIME
Time Change Difference: $DIFF
Change(s).............: $CHANGE
Change Type(s)........: $TYPE
Error Information.....: $ERROR

`eval $CMD`
EOF

exit $?;
